use yew::prelude::*;
use crate::api;
use crate::types::{Product};
use anyhow::Error;
use yew::format::Json;
use yew::services::fetch::FetchTask;
use yew::services::ConsoleService;

struct State {
    products: Vec<Product>,
    get_products_error: Option<Error>,
    get_products_loaded: bool
}

pub struct Home {
    state: State,
    link: ComponentLink<Self>,
    task: Option<FetchTask>
}

pub enum Msg {
    GetProducts,
    GetProductsSuccess(Vec<Product>),
    GetProductsError(Error),
}

impl Component for Home {

    type Message = Msg;
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {

        let products: Vec<Product> = vec![];
        link.send_message(Msg::GetProducts);

        Self {
            state: State {
                products: products,
                get_products_error: None,
                get_products_loaded: false
            },
            link: link,
            task: None
        }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {

        match message {

            Msg::GetProducts => {

                self.state.get_products_loaded = false;
                let handler = self.link.callback(move|response: api::FetchResponse<Vec<Product>>| {
                    let(_, Json(data)) = response.into_parts();
                    match data {
                        Ok(products) => {
                            ConsoleService::info("Successfuly fetched product list");
                            Msg::GetProductsSuccess(products)
                        },
                        Err(err) => {
                            ConsoleService::error(&format!("Could not fetch product list {}", err));
                            Msg::GetProductsError(err)
                        },
                    }
                });
                self.task = Some(api::get_products(handler));
                true
            }
            Msg::GetProductsSuccess(products) => {
                self.state.products = products;
                self.state.get_products_loaded = true;
                true
            }
            Msg::GetProductsError(error) => {
                self.state.get_products_error = Some(error);
                self.state.get_products_loaded = true;
                true
            }
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {

        let products: Vec<Html> = self.state.products.iter().map(|product: &Product| {
            html! {
                <div class="col-lg-6 col-md-12 text-center">
                    <div class="mt-5">
                        <div class="list-group-item">
                            <i class="fas fa-4x fa-gem text-primary mb-4"></i>
                        </div>
                        <div class="list-group-item">
                            <h3 class="h4 mb-2">{&product.punchline}</h3>
                        </div>
                        <div class="list-group-item">
                            <p class="text-muted mb-0">{&product.headline}</p>
                        </div>
                        <div class="list-group-item text-left">
                            <ol class="list-group text-justified">
                               {product.content.iter().enumerate().map(|(index, item)| {
                                    html!{
                                        <li class="list-group-item">{index+1}{{"). "}} {item}</li>
                                    }
                                }).collect::<Html>()}
                            </ol>
                        </div>
                    </div>
                </div>
            }
        }).collect();

        let mut images: Vec<Html>=Vec::new();
        for i in 1..24 {
            let html: Html = html! {
                <div class="col-lg-2 col-sm-2 border border-secondary">
                    <a class="portfolio-box" href=format!("assets/img/portfolio/fullsize/{}.jpg", {i})>
                        <img  style="width:160px;height:100px;" class="img-fluid" src=format!("assets/img/portfolio/thumbnails/thumb-{}.jpg", {i}) alt="" />
                    </a>
                </div>
            };
            ConsoleService::info(&format!("{}",{i}));
            images.push(html);
        }

        html! {
        <>
        //<!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">{"Energize Your Life"}</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto my-2 my-lg-0">
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#masthead">{"Home"}</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">{"About"}</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#services">{"Services"}</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portfolio">{"Portfolio"}</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">{"Contact"}</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        //<!-- Masthead-->
        <header class="masthead" id="masthead">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-center text-center">
                    <div class="col-lg-10 align-self-end">
                        <h1 class="text-capitalize text-white font-weight-bold">{"Brain needs Martial Arts as Much as it need Oxygen!"}</h1>
                        <hr class="divider my-4" />
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                        <p class="text-white-75 font-weight-light mb-5">{"\"Fearlessness is like a muscle. I know from my own life that the more I exercise it the more natural it becomes to not let my fears run me.\" – Arianna Huffington"}</p>
                        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">{"Find Out More"}</a>
                    </div>
                </div>
            </div>
        </header>
        //<!-- About-->
        <section class="page-section bg-primary" id="about">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h2 class="text-white mt-0">{"We've got what you need!"}</h2>
                        <hr class="divider light my-4" />
                        <p class="text-white-50 mb-4">{"Naga Martial academy is a place where various physical \
                        and artistic courses are provided with their complete glory!, \n
                        We love to see you in our family \
                        of lovely people who realized the importance of living life to the fullest,"}</p>
                        <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">{"Get Started!"}</a>
                    </div>
                </div>
            </div>
        </section>
        //<!-- Services-->

        <section class="page-section" id="services">
            <div class="container">
                <h2 class="text-center mt-0">{"Our Most advanced courses"}</h2>
                <hr class="divider my-4" />
                <div class="row">
                    {products}
                </div>
            </div>
        </section>

        //<!-- Portfolio-->
        <div id="portfolio">
            <div class="container">
                <div class="row">
                    {images}
                </div>
            </div>
        </div>
        //<!-- Call to action-->
        <section class="page-section bg-dark text-white">
            <div class="container text-center">
                <h2 class="mb-4">{"Life is good only with dancing, singing and martial arts!"}</h2>
                <a class="btn btn-light btn-xl" href="#contact">{"Contact us"}</a>
            </div>
        </section>
        //<!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h2 class="mt-0">{"Let's Get In Touch!"}</h2>
                        <hr class="divider my-4" />
                        <p class="text-muted mb-5">{"Ready to energize your life with body, mind & soul with rejuvenating and relaxing physical exercises, music & martial arts?"}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                        <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                        <div>{"+91 (988) 036-1209"}</div>
                    </div>
                    <div class="col-lg-4 mr-auto text-center">
                        <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
                        //<!-- Make sure to change the email address in BOTH the anchor text and the link target below!-->
                        <a class="d-block" href="mailto:siddukarate1977@gmail.com">{"siddukarate1977@gmail.com"}</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col ml-auto text-center mb-5 mb-lg-0">
                        <i class="fas fa-home fa-3x mb-3 text-muted"></i>
                        <div>{"Naaga Martial Academy"}</div>
                        <div>{"#2025, 1st Floor, 80 feet Road"}</div>
                        <div>{"F Block, 3rd Stage, Dattagalli"}</div>
                        <div>{"Mysore - 570023"}</div>
                    </div>
                </div>
            </div>
        </section>
        //<!-- Footer-->
        <footer class="bg-light py-5">
            <div class="container"><div class="small text-center text-muted">{"Copyright © 2020 - Naga Martial Academy"}</div></div>
        </footer>
        //<!-- Bootstrap core JS-->
        //<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        //<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        ////<!-- Third party plugin JS-->
        //<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        //<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        ////<!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        </>
        }
    }
}
