
use yew::prelude::*;
use yew_router::prelude::*;

use crate::pages::Home;
use crate::route::Route;


pub struct App { link: ComponentLink<Self> }


impl Component for App {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {

        Self {
            link,
        }
    }

    fn update(&mut self, message: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {

        let render = Router::render(move |switch: Route|
            match switch {
            Route::HomePage => {
                html! {<Home/>}
            }
        });

        html! {
            <>
                <Router<Route, ()> render=render/>
            </>
        }
    }
}
