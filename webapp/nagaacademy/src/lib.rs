#![recursion_limit="2056"]
mod pages;
mod types;
mod components;
mod app;
mod api;
mod route;

use wasm_bindgen::prelude::*;
use yew::prelude::*;

#[wasm_bindgen(start)]
pub fn run_app() {
    App::<app::App>::new().mount_to_body();
}
