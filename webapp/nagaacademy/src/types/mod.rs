use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Product {
    pub id: i32,
    pub headline: String,
    pub punchline: String,
    pub url: String,
    pub phone: String,
    pub place: String,
    pub note: String,
    pub footaddress: Vec<String>,
    pub content: Vec<String>
}
