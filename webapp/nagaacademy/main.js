import init, { run_app } from './pkg/nagaacademy.js';
async function main() {
   await init('/nagaacademy_bg.wasm');
   run_app();
}
main()
