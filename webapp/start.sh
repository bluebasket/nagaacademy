#!/bin/bash

cp docker/DockerFileDev Dockerfile;

CONTAINER=$(docker ps| grep 'nagawebapp_service')

if [ ${#CONTAINER} -ge 5 ]; then
    echo "Continer is already running";
    echo "Entering Continer ........";
    docker exec -it nagawebapp_service /bin/bash;
    exit 1;
else
    echo "Continer not running";
fi

DATABASE=$(docker ps| grep 'nagapostgres_service')

if [ ${#DATABASE} -ge 5 ]; then
    echo "Database Exists";
else
    echo "Run Database container server name 'nagapostgres_service' and docker Network 'naga-net' first";
    exit 1
fi

IMAGE=$(docker images| grep 'nagawebappservice')

if [ ${#IMAGE} -ge 5 ]; then
    echo "Image Exists";
else
    echo "Build New Image";
    docker build -t nagawebappservice:latest .;
fi

docker run -it --network naga-net --name nagawebapp_service -p 8001:8000 -v '/home/spiderman/GitWorld/':'/home/spiderman/GitWorld' nagawebappservice:latest /bin/bash;

TAG_NUMBER=$(docker ps -a|grep 'nagawebapp_service'|awk '{ print $1}');
docker commit $TAG_NUMBER nagawebappservice:latest;
docker rm $TAG_NUMBER;

echo "----------------"
echo "If Quiting happened peacefully than all data is saved to image";
echo "----------------"
