#!/bin/bash

IMAGE=$(docker images| grep 'nagawebappservice')

if [ ${#IMAGE} -ge 5 ]; then
    docker run -v '/home/spiderman/GitWorld/':'/home/spiderman/GitWorld' nagawebappservice:latest cargo make release;
    echo "Release successfull";
else
    echo "Please run ./start.sh";
    echo "than run ./build_pro.sh";
    exit 1;
fi

cp docker/DockerFileProd Dockerfile;

CONTAINER=$(docker ps| grep 'nagawebapp_service_prod')

if [ ${#CONTAINER} -ge 5 ]; then
    echo "Continer is already running"  ;
    echo "Stopping Continer";
    docker stop nagawebapp_service_prod;
    docker rm nagawebapp_service_prod;
else
    docker rm nagawebapp_service_prod;
    echo "Continer not running";
fi

IMAGE=$(docker images| grep 'nagawebappserviceprod')

if [ ${#IMAGE} -ge 5 ]; then
    docker image rm nagawebappserviceprod;
    echo "Image Removed";
else
    echo "Build New Image";
fi

docker build -t nagawebappserviceprod:latest .

echo "----------------"
echo "Image management";
echo "----------------"

#docker run -it --rm --network bat-net --name nagawebapp_service_prod nagawebappserviceprod:latest /bin/ash
docker run --rm --network naga-net --name nagawebapp_service_prod -p 8012:8012 nagawebappserviceprod:latest;
